'use strict';
 /*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
/**
 * AWS Lambda Function proxy delegate functions.
 * <p>
 * These functions implement the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the availability of related services in the case of a business related service failing.</li>
 * <li>Exposes a simpler, uniform interface to the business tier, making it easy for clients to consume a business data.</li>
 * <li>Hides the communication protocol that may be required to fulfill business related services.</li>
 * <li>Hides the details of how business data is persisted.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */

/**
 * function to create the provided Referrer data raising an exception if it fails
 */
exports.createReferrer = function(event, context, callback) {	
	var businessObject = event.body;
		
	if ( businessObject == null ) {
        var errMsg = "error on createReferrer - null Referrer provided but not allowed. ";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
  
    try {
        innerCreateReferrer( businessObject, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createReferrer - unable to create Referrer" + getContextDetails(context, err);
    	console.log( errMsg );
        callback( new Error( errMsg ) );
    }     
};

function innerCreateReferrer( businessObject, event, context, callback ) {
	try {
        var actionName 		= "save";
		callPut("Referrer", actionName, businessObject, event, callback);
	} catch (err) {
		console.log( "exception caught in innerCreateReferrer - " + getContextDetails(context, err) );
		callback( err );
	}
};

/**
 * function to retrieve the Referrer data via a supplied Referrer primary key.
 */
exports.getReferrer = function(event, context, callback) {
    var key = 	{ 
    				referrerId : 0
    			};                
	
	key.referrerId = event.referrerId;
    
    try {
        innerGetReferrer( key, event, context, callback);        
    }
    catch( err ) {
        var errMsg = "err+or on createReferrer - unable to locate Referrer with key " 
        	+ key.toString() + " - " + getContextDetails(context, err);
    	console.log( errMsg );
        callback(  new Error( errMsg ) );
    }
};

function innerGetReferrer( primaryKey, event, context, callback ) {
	try {
        var actionName	= "load";
		callGet("Referrer", actionName, primaryKey, event, callback);
	} catch (err) {
		console.log( "exception caught in innerGetReferrer - " + getContextDetails(context, err) );
		throw err;
	}
};

     
/**
 * function to save the provided Referrer data.
 */
exports.saveReferrer = function(event, context, callback) {
    var businessObject = event.body;                

	if ( !exists( businessObject ) ) {
        var errMsg = "error on createReferrer - null Referrer provided but not allowed.";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
	                
    if ( exists( businessObject.referrerId ) ) {
        try {                    
            innerSaveReferrer(businessObject, event, context, callback);
        }
        catch (err) {
        	var errMsg = "error on createReferrer - unable to save Referrer " 
        		+ " - " + getContextDetails(context, err);
    		console.log( errMsg );
        	callback( new Error( errMsg ) );        
        }
    }
    else {
        var errMsg = "Unable to create Referrer due to it having a null ReferrerPrimaryKey." 
        				+ " - " + getContextDetails(context)
        console.log( errMsg );
        callback( new Error( errMsg ) );
    }
    
};
     
function innerSaveReferrer( businessObject, event, context, callback ) {
	try {
		var actionName	= "save";
        callPut("Referrer", actionName, businessObject, event, callback);
	}
	catch ( err ) {
		console.log( "exception caught in innerSaveReferrer - " + getContextDetails(context, err) );
		throw err;
	}
};

/**
 * function to retrieve a all Referrers
 */
exports.getAllReferrer = function(event, context, callback) {
    try {
        var actionName = "viewAll";
        innerGetAllReferrer(event, context, callback);  
    } catch( err ) {
    	var errMsg = "error on createReferrer - failed to getAllReferrer " 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};
           
function innerGetAllReferrer(event, context, callback) {
	try {
		var actionName = "viewAll";
        callGet("Referrer", actionName, null, event, callback);	
	} catch( err ) {
		console.log( "exception caught in innerGetAllReferrer - " + getContextDetails(context, err) );
	    throw err;
	}
}; 
     
/**
 * function to delete the associated Referrer using its provided primary key.
 */
exports.deleteReferrer = function(event, context, callback) {
    var key = 	{ 
    				referrerId : 0
    			};                
	
	key.referrerId = event.referrerId;
	
    try {                    
       	innerDeleteReferrer(key, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createReferrer - Unable to delete Referrer using key = "  
    		+ key 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};

function innerDeleteReferrer( primaryKey, event, context, callback) {
	try {
		var actionName 	= "delete";
        callGet("Referrer", actionName, primaryKey, event, callback );
	} catch ( err ) {
		console.log( "exception caught in innerDeleteReferrer - " + getContextDetails(context, err) );
		throw err;
	}
};

// role related methods

 
/**
 * function to retrieve the Comments on a Referrer
 * @param parentKey
 */
exports.getCommentsOnReferrer = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Referrer", "loadComments", keys, event, callback);
};
    
/**
 * function to save multiple Comment entities as the Comments 
 * of the relevant Referrer associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.assignCommentsOnReferrer = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Referrer", "saveComments", keys, event, callback);
};

/**
 * function to delete multiple Comment entities as the Comments 
 * of the relevant Referrer associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.deleteCommentsOnReferrer = function(event, context, callback) {		
    var keys = 	event.body;
    callGet("Referrer", "deleteComments", keys, event, callback);
};

 

/**
 * function to create the provided TheReference data raising an exception if it fails
 */
exports.createTheReference = function(event, context, callback) {	
	var businessObject = event.body;
		
	if ( businessObject == null ) {
        var errMsg = "error on createTheReference - null TheReference provided but not allowed. ";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
  
    try {
        innerCreateTheReference( businessObject, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createTheReference - unable to create TheReference" + getContextDetails(context, err);
    	console.log( errMsg );
        callback( new Error( errMsg ) );
    }     
};

function innerCreateTheReference( businessObject, event, context, callback ) {
	try {
        var actionName 		= "save";
		callPut("TheReference", actionName, businessObject, event, callback);
	} catch (err) {
		console.log( "exception caught in innerCreateTheReference - " + getContextDetails(context, err) );
		callback( err );
	}
};

/**
 * function to retrieve the TheReference data via a supplied TheReference primary key.
 */
exports.getTheReference = function(event, context, callback) {
    var key = 	{ 
    				theReferenceId : 0
    			};                
	
	key.theReferenceId = event.theReferenceId;
    
    try {
        innerGetTheReference( key, event, context, callback);        
    }
    catch( err ) {
        var errMsg = "err+or on createTheReference - unable to locate TheReference with key " 
        	+ key.toString() + " - " + getContextDetails(context, err);
    	console.log( errMsg );
        callback(  new Error( errMsg ) );
    }
};

function innerGetTheReference( primaryKey, event, context, callback ) {
	try {
        var actionName	= "load";
		callGet("TheReference", actionName, primaryKey, event, callback);
	} catch (err) {
		console.log( "exception caught in innerGetTheReference - " + getContextDetails(context, err) );
		throw err;
	}
};

     
/**
 * function to save the provided TheReference data.
 */
exports.saveTheReference = function(event, context, callback) {
    var businessObject = event.body;                

	if ( !exists( businessObject ) ) {
        var errMsg = "error on createTheReference - null TheReference provided but not allowed.";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
	                
    if ( exists( businessObject.theReferenceId ) ) {
        try {                    
            innerSaveTheReference(businessObject, event, context, callback);
        }
        catch (err) {
        	var errMsg = "error on createTheReference - unable to save TheReference " 
        		+ " - " + getContextDetails(context, err);
    		console.log( errMsg );
        	callback( new Error( errMsg ) );        
        }
    }
    else {
        var errMsg = "Unable to create TheReference due to it having a null TheReferencePrimaryKey." 
        				+ " - " + getContextDetails(context)
        console.log( errMsg );
        callback( new Error( errMsg ) );
    }
    
};
     
function innerSaveTheReference( businessObject, event, context, callback ) {
	try {
		var actionName	= "save";
        callPut("TheReference", actionName, businessObject, event, callback);
	}
	catch ( err ) {
		console.log( "exception caught in innerSaveTheReference - " + getContextDetails(context, err) );
		throw err;
	}
};

/**
 * function to retrieve a all TheReferences
 */
exports.getAllTheReference = function(event, context, callback) {
    try {
        var actionName = "viewAll";
        innerGetAllTheReference(event, context, callback);  
    } catch( err ) {
    	var errMsg = "error on createTheReference - failed to getAllTheReference " 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};
           
function innerGetAllTheReference(event, context, callback) {
	try {
		var actionName = "viewAll";
        callGet("TheReference", actionName, null, event, callback);	
	} catch( err ) {
		console.log( "exception caught in innerGetAllTheReference - " + getContextDetails(context, err) );
	    throw err;
	}
}; 
     
/**
 * function to delete the associated TheReference using its provided primary key.
 */
exports.deleteTheReference = function(event, context, callback) {
    var key = 	{ 
    				theReferenceId : 0
    			};                
	
	key.theReferenceId = event.theReferenceId;
	
    try {                    
       	innerDeleteTheReference(key, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createTheReference - Unable to delete TheReference using key = "  
    		+ key 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};

function innerDeleteTheReference( primaryKey, event, context, callback) {
	try {
		var actionName 	= "delete";
        callGet("TheReference", actionName, primaryKey, event, callback );
	} catch ( err ) {
		console.log( "exception caught in innerDeleteTheReference - " + getContextDetails(context, err) );
		throw err;
	}
};

// role related methods

/**
 * function to get the QuestionGroup using the provided primary key of a TheReference
 */
exports.getQuestionGroupOnTheReference = function(event, context, callback) {	
    var keys = 	event.body;
    callGet("TheReference", "loadQuestionGroup", keys, event, callback );
};

/**
 * function to assign the QuestionGroup on a TheReference using the provided primary key of a QuestionGroup
 */
exports.saveQuestionGroupOnTheReference = function(event, context, callback) {
    var keys = 	event.body;
    callGet("TheReference", "saveQuestionGroup", keys, event, callback);
};

/**
 * function to remove the assignment of the QuestionGroup on a TheReference
 */
exports.deleteQuestionGroupOnTheReference = function(event, context, callback) {
    var keys = 	event.body;
    callGet("TheReference", "deleteQuestionGroup", keys, event, callback);
};
		
/**
 * function to get the User using the provided primary key of a TheReference
 */
exports.getUserOnTheReference = function(event, context, callback) {	
    var keys = 	event.body;
    callGet("TheReference", "loadUser", keys, event, callback );
};

/**
 * function to assign the User on a TheReference using the provided primary key of a User
 */
exports.saveUserOnTheReference = function(event, context, callback) {
    var keys = 	event.body;
    callGet("TheReference", "saveUser", keys, event, callback);
};

/**
 * function to remove the assignment of the User on a TheReference
 */
exports.deleteUserOnTheReference = function(event, context, callback) {
    var keys = 	event.body;
    callGet("TheReference", "deleteUser", keys, event, callback);
};
		
/**
 * function to get the Referrer using the provided primary key of a TheReference
 */
exports.getReferrerOnTheReference = function(event, context, callback) {	
    var keys = 	event.body;
    callGet("TheReference", "loadReferrer", keys, event, callback );
};

/**
 * function to assign the Referrer on a TheReference using the provided primary key of a Referrer
 */
exports.saveReferrerOnTheReference = function(event, context, callback) {
    var keys = 	event.body;
    callGet("TheReference", "saveReferrer", keys, event, callback);
};

/**
 * function to remove the assignment of the Referrer on a TheReference
 */
exports.deleteReferrerOnTheReference = function(event, context, callback) {
    var keys = 	event.body;
    callGet("TheReference", "deleteReferrer", keys, event, callback);
};
		
/**
 * function to get the LastQuestionAnswered using the provided primary key of a TheReference
 */
exports.getLastQuestionAnsweredOnTheReference = function(event, context, callback) {	
    var keys = 	event.body;
    callGet("TheReference", "loadLastQuestionAnswered", keys, event, callback );
};

/**
 * function to assign the LastQuestionAnswered on a TheReference using the provided primary key of a Question
 */
exports.saveLastQuestionAnsweredOnTheReference = function(event, context, callback) {
    var keys = 	event.body;
    callGet("TheReference", "saveLastQuestionAnswered", keys, event, callback);
};

/**
 * function to remove the assignment of the LastQuestionAnswered on a TheReference
 */
exports.deleteLastQuestionAnsweredOnTheReference = function(event, context, callback) {
    var keys = 	event.body;
    callGet("TheReference", "deleteLastQuestionAnswered", keys, event, callback);
};
		
 
/**
 * function to retrieve the Answers on a TheReference
 * @param parentKey
 */
exports.getAnswersOnTheReference = function(event, context, callback) {
    var keys = 	event.body;
    callGet("TheReference", "loadAnswers", keys, event, callback);
};
    
/**
 * function to save multiple Answer entities as the Answers 
 * of the relevant TheReference associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.assignAnswersOnTheReference = function(event, context, callback) {
    var keys = 	event.body;
    callGet("TheReference", "saveAnswers", keys, event, callback);
};

/**
 * function to delete multiple Answer entities as the Answers 
 * of the relevant TheReference associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.deleteAnswersOnTheReference = function(event, context, callback) {		
    var keys = 	event.body;
    callGet("TheReference", "deleteAnswers", keys, event, callback);
};

 

/**
 * function to create the provided ReferrerGroup data raising an exception if it fails
 */
exports.createReferrerGroup = function(event, context, callback) {	
	var businessObject = event.body;
		
	if ( businessObject == null ) {
        var errMsg = "error on createReferrerGroup - null ReferrerGroup provided but not allowed. ";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
  
    try {
        innerCreateReferrerGroup( businessObject, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createReferrerGroup - unable to create ReferrerGroup" + getContextDetails(context, err);
    	console.log( errMsg );
        callback( new Error( errMsg ) );
    }     
};

function innerCreateReferrerGroup( businessObject, event, context, callback ) {
	try {
        var actionName 		= "save";
		callPut("ReferrerGroup", actionName, businessObject, event, callback);
	} catch (err) {
		console.log( "exception caught in innerCreateReferrerGroup - " + getContextDetails(context, err) );
		callback( err );
	}
};

/**
 * function to retrieve the ReferrerGroup data via a supplied ReferrerGroup primary key.
 */
exports.getReferrerGroup = function(event, context, callback) {
    var key = 	{ 
    				referrerGroupId : 0
    			};                
	
	key.referrerGroupId = event.referrerGroupId;
    
    try {
        innerGetReferrerGroup( key, event, context, callback);        
    }
    catch( err ) {
        var errMsg = "err+or on createReferrerGroup - unable to locate ReferrerGroup with key " 
        	+ key.toString() + " - " + getContextDetails(context, err);
    	console.log( errMsg );
        callback(  new Error( errMsg ) );
    }
};

function innerGetReferrerGroup( primaryKey, event, context, callback ) {
	try {
        var actionName	= "load";
		callGet("ReferrerGroup", actionName, primaryKey, event, callback);
	} catch (err) {
		console.log( "exception caught in innerGetReferrerGroup - " + getContextDetails(context, err) );
		throw err;
	}
};

     
/**
 * function to save the provided ReferrerGroup data.
 */
exports.saveReferrerGroup = function(event, context, callback) {
    var businessObject = event.body;                

	if ( !exists( businessObject ) ) {
        var errMsg = "error on createReferrerGroup - null ReferrerGroup provided but not allowed.";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
	                
    if ( exists( businessObject.referrerGroupId ) ) {
        try {                    
            innerSaveReferrerGroup(businessObject, event, context, callback);
        }
        catch (err) {
        	var errMsg = "error on createReferrerGroup - unable to save ReferrerGroup " 
        		+ " - " + getContextDetails(context, err);
    		console.log( errMsg );
        	callback( new Error( errMsg ) );        
        }
    }
    else {
        var errMsg = "Unable to create ReferrerGroup due to it having a null ReferrerGroupPrimaryKey." 
        				+ " - " + getContextDetails(context)
        console.log( errMsg );
        callback( new Error( errMsg ) );
    }
    
};
     
function innerSaveReferrerGroup( businessObject, event, context, callback ) {
	try {
		var actionName	= "save";
        callPut("ReferrerGroup", actionName, businessObject, event, callback);
	}
	catch ( err ) {
		console.log( "exception caught in innerSaveReferrerGroup - " + getContextDetails(context, err) );
		throw err;
	}
};

/**
 * function to retrieve a all ReferrerGroups
 */
exports.getAllReferrerGroup = function(event, context, callback) {
    try {
        var actionName = "viewAll";
        innerGetAllReferrerGroup(event, context, callback);  
    } catch( err ) {
    	var errMsg = "error on createReferrerGroup - failed to getAllReferrerGroup " 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};
           
function innerGetAllReferrerGroup(event, context, callback) {
	try {
		var actionName = "viewAll";
        callGet("ReferrerGroup", actionName, null, event, callback);	
	} catch( err ) {
		console.log( "exception caught in innerGetAllReferrerGroup - " + getContextDetails(context, err) );
	    throw err;
	}
}; 
     
/**
 * function to delete the associated ReferrerGroup using its provided primary key.
 */
exports.deleteReferrerGroup = function(event, context, callback) {
    var key = 	{ 
    				referrerGroupId : 0
    			};                
	
	key.referrerGroupId = event.referrerGroupId;
	
    try {                    
       	innerDeleteReferrerGroup(key, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createReferrerGroup - Unable to delete ReferrerGroup using key = "  
    		+ key 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};

function innerDeleteReferrerGroup( primaryKey, event, context, callback) {
	try {
		var actionName 	= "delete";
        callGet("ReferrerGroup", actionName, primaryKey, event, callback );
	} catch ( err ) {
		console.log( "exception caught in innerDeleteReferrerGroup - " + getContextDetails(context, err) );
		throw err;
	}
};

// role related methods

 
/**
 * function to retrieve the References on a ReferrerGroup
 * @param parentKey
 */
exports.getReferencesOnReferrerGroup = function(event, context, callback) {
    var keys = 	event.body;
    callGet("ReferrerGroup", "loadReferences", keys, event, callback);
};
    
/**
 * function to save multiple TheReference entities as the References 
 * of the relevant ReferrerGroup associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.assignReferencesOnReferrerGroup = function(event, context, callback) {
    var keys = 	event.body;
    callGet("ReferrerGroup", "saveReferences", keys, event, callback);
};

/**
 * function to delete multiple TheReference entities as the References 
 * of the relevant ReferrerGroup associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.deleteReferencesOnReferrerGroup = function(event, context, callback) {		
    var keys = 	event.body;
    callGet("ReferrerGroup", "deleteReferences", keys, event, callback);
};

 

/**
 * function to create the provided ReferenceEngine data raising an exception if it fails
 */
exports.createReferenceEngine = function(event, context, callback) {	
	var businessObject = event.body;
		
	if ( businessObject == null ) {
        var errMsg = "error on createReferenceEngine - null ReferenceEngine provided but not allowed. ";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
  
    try {
        innerCreateReferenceEngine( businessObject, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createReferenceEngine - unable to create ReferenceEngine" + getContextDetails(context, err);
    	console.log( errMsg );
        callback( new Error( errMsg ) );
    }     
};

function innerCreateReferenceEngine( businessObject, event, context, callback ) {
	try {
        var actionName 		= "save";
		callPut("ReferenceEngine", actionName, businessObject, event, callback);
	} catch (err) {
		console.log( "exception caught in innerCreateReferenceEngine - " + getContextDetails(context, err) );
		callback( err );
	}
};

/**
 * function to retrieve the ReferenceEngine data via a supplied ReferenceEngine primary key.
 */
exports.getReferenceEngine = function(event, context, callback) {
    var key = 	{ 
    				referenceEngineId : 0
    			};                
	
	key.referenceEngineId = event.referenceEngineId;
    
    try {
        innerGetReferenceEngine( key, event, context, callback);        
    }
    catch( err ) {
        var errMsg = "err+or on createReferenceEngine - unable to locate ReferenceEngine with key " 
        	+ key.toString() + " - " + getContextDetails(context, err);
    	console.log( errMsg );
        callback(  new Error( errMsg ) );
    }
};

function innerGetReferenceEngine( primaryKey, event, context, callback ) {
	try {
        var actionName	= "load";
		callGet("ReferenceEngine", actionName, primaryKey, event, callback);
	} catch (err) {
		console.log( "exception caught in innerGetReferenceEngine - " + getContextDetails(context, err) );
		throw err;
	}
};

     
/**
 * function to save the provided ReferenceEngine data.
 */
exports.saveReferenceEngine = function(event, context, callback) {
    var businessObject = event.body;                

	if ( !exists( businessObject ) ) {
        var errMsg = "error on createReferenceEngine - null ReferenceEngine provided but not allowed.";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
	                
    if ( exists( businessObject.referenceEngineId ) ) {
        try {                    
            innerSaveReferenceEngine(businessObject, event, context, callback);
        }
        catch (err) {
        	var errMsg = "error on createReferenceEngine - unable to save ReferenceEngine " 
        		+ " - " + getContextDetails(context, err);
    		console.log( errMsg );
        	callback( new Error( errMsg ) );        
        }
    }
    else {
        var errMsg = "Unable to create ReferenceEngine due to it having a null ReferenceEnginePrimaryKey." 
        				+ " - " + getContextDetails(context)
        console.log( errMsg );
        callback( new Error( errMsg ) );
    }
    
};
     
function innerSaveReferenceEngine( businessObject, event, context, callback ) {
	try {
		var actionName	= "save";
        callPut("ReferenceEngine", actionName, businessObject, event, callback);
	}
	catch ( err ) {
		console.log( "exception caught in innerSaveReferenceEngine - " + getContextDetails(context, err) );
		throw err;
	}
};

/**
 * function to retrieve a all ReferenceEngines
 */
exports.getAllReferenceEngine = function(event, context, callback) {
    try {
        var actionName = "viewAll";
        innerGetAllReferenceEngine(event, context, callback);  
    } catch( err ) {
    	var errMsg = "error on createReferenceEngine - failed to getAllReferenceEngine " 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};
           
function innerGetAllReferenceEngine(event, context, callback) {
	try {
		var actionName = "viewAll";
        callGet("ReferenceEngine", actionName, null, event, callback);	
	} catch( err ) {
		console.log( "exception caught in innerGetAllReferenceEngine - " + getContextDetails(context, err) );
	    throw err;
	}
}; 
     
/**
 * function to delete the associated ReferenceEngine using its provided primary key.
 */
exports.deleteReferenceEngine = function(event, context, callback) {
    var key = 	{ 
    				referenceEngineId : 0
    			};                
	
	key.referenceEngineId = event.referenceEngineId;
	
    try {                    
       	innerDeleteReferenceEngine(key, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createReferenceEngine - Unable to delete ReferenceEngine using key = "  
    		+ key 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};

function innerDeleteReferenceEngine( primaryKey, event, context, callback) {
	try {
		var actionName 	= "delete";
        callGet("ReferenceEngine", actionName, primaryKey, event, callback );
	} catch ( err ) {
		console.log( "exception caught in innerDeleteReferenceEngine - " + getContextDetails(context, err) );
		throw err;
	}
};

// role related methods

/**
 * function to get the MainQuestionGroup using the provided primary key of a ReferenceEngine
 */
exports.getMainQuestionGroupOnReferenceEngine = function(event, context, callback) {	
    var keys = 	event.body;
    callGet("ReferenceEngine", "loadMainQuestionGroup", keys, event, callback );
};

/**
 * function to assign the MainQuestionGroup on a ReferenceEngine using the provided primary key of a QuestionGroup
 */
exports.saveMainQuestionGroupOnReferenceEngine = function(event, context, callback) {
    var keys = 	event.body;
    callGet("ReferenceEngine", "saveMainQuestionGroup", keys, event, callback);
};

/**
 * function to remove the assignment of the MainQuestionGroup on a ReferenceEngine
 */
exports.deleteMainQuestionGroupOnReferenceEngine = function(event, context, callback) {
    var keys = 	event.body;
    callGet("ReferenceEngine", "deleteMainQuestionGroup", keys, event, callback);
};
		
 
 

/**
 * function to create the provided Question data raising an exception if it fails
 */
exports.createQuestion = function(event, context, callback) {	
	var businessObject = event.body;
		
	if ( businessObject == null ) {
        var errMsg = "error on createQuestion - null Question provided but not allowed. ";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
  
    try {
        innerCreateQuestion( businessObject, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createQuestion - unable to create Question" + getContextDetails(context, err);
    	console.log( errMsg );
        callback( new Error( errMsg ) );
    }     
};

function innerCreateQuestion( businessObject, event, context, callback ) {
	try {
        var actionName 		= "save";
		callPut("Question", actionName, businessObject, event, callback);
	} catch (err) {
		console.log( "exception caught in innerCreateQuestion - " + getContextDetails(context, err) );
		callback( err );
	}
};

/**
 * function to retrieve the Question data via a supplied Question primary key.
 */
exports.getQuestion = function(event, context, callback) {
    var key = 	{ 
    				questionId : 0
    			};                
	
	key.questionId = event.questionId;
    
    try {
        innerGetQuestion( key, event, context, callback);        
    }
    catch( err ) {
        var errMsg = "err+or on createQuestion - unable to locate Question with key " 
        	+ key.toString() + " - " + getContextDetails(context, err);
    	console.log( errMsg );
        callback(  new Error( errMsg ) );
    }
};

function innerGetQuestion( primaryKey, event, context, callback ) {
	try {
        var actionName	= "load";
		callGet("Question", actionName, primaryKey, event, callback);
	} catch (err) {
		console.log( "exception caught in innerGetQuestion - " + getContextDetails(context, err) );
		throw err;
	}
};

     
/**
 * function to save the provided Question data.
 */
exports.saveQuestion = function(event, context, callback) {
    var businessObject = event.body;                

	if ( !exists( businessObject ) ) {
        var errMsg = "error on createQuestion - null Question provided but not allowed.";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
	                
    if ( exists( businessObject.questionId ) ) {
        try {                    
            innerSaveQuestion(businessObject, event, context, callback);
        }
        catch (err) {
        	var errMsg = "error on createQuestion - unable to save Question " 
        		+ " - " + getContextDetails(context, err);
    		console.log( errMsg );
        	callback( new Error( errMsg ) );        
        }
    }
    else {
        var errMsg = "Unable to create Question due to it having a null QuestionPrimaryKey." 
        				+ " - " + getContextDetails(context)
        console.log( errMsg );
        callback( new Error( errMsg ) );
    }
    
};
     
function innerSaveQuestion( businessObject, event, context, callback ) {
	try {
		var actionName	= "save";
        callPut("Question", actionName, businessObject, event, callback);
	}
	catch ( err ) {
		console.log( "exception caught in innerSaveQuestion - " + getContextDetails(context, err) );
		throw err;
	}
};

/**
 * function to retrieve a all Questions
 */
exports.getAllQuestion = function(event, context, callback) {
    try {
        var actionName = "viewAll";
        innerGetAllQuestion(event, context, callback);  
    } catch( err ) {
    	var errMsg = "error on createQuestion - failed to getAllQuestion " 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};
           
function innerGetAllQuestion(event, context, callback) {
	try {
		var actionName = "viewAll";
        callGet("Question", actionName, null, event, callback);	
	} catch( err ) {
		console.log( "exception caught in innerGetAllQuestion - " + getContextDetails(context, err) );
	    throw err;
	}
}; 
     
/**
 * function to delete the associated Question using its provided primary key.
 */
exports.deleteQuestion = function(event, context, callback) {
    var key = 	{ 
    				questionId : 0
    			};                
	
	key.questionId = event.questionId;
	
    try {                    
       	innerDeleteQuestion(key, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createQuestion - Unable to delete Question using key = "  
    		+ key 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};

function innerDeleteQuestion( primaryKey, event, context, callback) {
	try {
		var actionName 	= "delete";
        callGet("Question", actionName, primaryKey, event, callback );
	} catch ( err ) {
		console.log( "exception caught in innerDeleteQuestion - " + getContextDetails(context, err) );
		throw err;
	}
};

// role related methods

 
/**
 * function to retrieve the Responses on a Question
 * @param parentKey
 */
exports.getResponsesOnQuestion = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Question", "loadResponses", keys, event, callback);
};
    
/**
 * function to save multiple TheResponse entities as the Responses 
 * of the relevant Question associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.assignResponsesOnQuestion = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Question", "saveResponses", keys, event, callback);
};

/**
 * function to delete multiple TheResponse entities as the Responses 
 * of the relevant Question associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.deleteResponsesOnQuestion = function(event, context, callback) {		
    var keys = 	event.body;
    callGet("Question", "deleteResponses", keys, event, callback);
};

 

/**
 * function to create the provided TheResponse data raising an exception if it fails
 */
exports.createTheResponse = function(event, context, callback) {	
	var businessObject = event.body;
		
	if ( businessObject == null ) {
        var errMsg = "error on createTheResponse - null TheResponse provided but not allowed. ";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
  
    try {
        innerCreateTheResponse( businessObject, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createTheResponse - unable to create TheResponse" + getContextDetails(context, err);
    	console.log( errMsg );
        callback( new Error( errMsg ) );
    }     
};

function innerCreateTheResponse( businessObject, event, context, callback ) {
	try {
        var actionName 		= "save";
		callPut("TheResponse", actionName, businessObject, event, callback);
	} catch (err) {
		console.log( "exception caught in innerCreateTheResponse - " + getContextDetails(context, err) );
		callback( err );
	}
};

/**
 * function to retrieve the TheResponse data via a supplied TheResponse primary key.
 */
exports.getTheResponse = function(event, context, callback) {
    var key = 	{ 
    				theResponseId : 0
    			};                
	
	key.theResponseId = event.theResponseId;
    
    try {
        innerGetTheResponse( key, event, context, callback);        
    }
    catch( err ) {
        var errMsg = "err+or on createTheResponse - unable to locate TheResponse with key " 
        	+ key.toString() + " - " + getContextDetails(context, err);
    	console.log( errMsg );
        callback(  new Error( errMsg ) );
    }
};

function innerGetTheResponse( primaryKey, event, context, callback ) {
	try {
        var actionName	= "load";
		callGet("TheResponse", actionName, primaryKey, event, callback);
	} catch (err) {
		console.log( "exception caught in innerGetTheResponse - " + getContextDetails(context, err) );
		throw err;
	}
};

     
/**
 * function to save the provided TheResponse data.
 */
exports.saveTheResponse = function(event, context, callback) {
    var businessObject = event.body;                

	if ( !exists( businessObject ) ) {
        var errMsg = "error on createTheResponse - null TheResponse provided but not allowed.";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
	                
    if ( exists( businessObject.theResponseId ) ) {
        try {                    
            innerSaveTheResponse(businessObject, event, context, callback);
        }
        catch (err) {
        	var errMsg = "error on createTheResponse - unable to save TheResponse " 
        		+ " - " + getContextDetails(context, err);
    		console.log( errMsg );
        	callback( new Error( errMsg ) );        
        }
    }
    else {
        var errMsg = "Unable to create TheResponse due to it having a null TheResponsePrimaryKey." 
        				+ " - " + getContextDetails(context)
        console.log( errMsg );
        callback( new Error( errMsg ) );
    }
    
};
     
function innerSaveTheResponse( businessObject, event, context, callback ) {
	try {
		var actionName	= "save";
        callPut("TheResponse", actionName, businessObject, event, callback);
	}
	catch ( err ) {
		console.log( "exception caught in innerSaveTheResponse - " + getContextDetails(context, err) );
		throw err;
	}
};

/**
 * function to retrieve a all TheResponses
 */
exports.getAllTheResponse = function(event, context, callback) {
    try {
        var actionName = "viewAll";
        innerGetAllTheResponse(event, context, callback);  
    } catch( err ) {
    	var errMsg = "error on createTheResponse - failed to getAllTheResponse " 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};
           
function innerGetAllTheResponse(event, context, callback) {
	try {
		var actionName = "viewAll";
        callGet("TheResponse", actionName, null, event, callback);	
	} catch( err ) {
		console.log( "exception caught in innerGetAllTheResponse - " + getContextDetails(context, err) );
	    throw err;
	}
}; 
     
/**
 * function to delete the associated TheResponse using its provided primary key.
 */
exports.deleteTheResponse = function(event, context, callback) {
    var key = 	{ 
    				theResponseId : 0
    			};                
	
	key.theResponseId = event.theResponseId;
	
    try {                    
       	innerDeleteTheResponse(key, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createTheResponse - Unable to delete TheResponse using key = "  
    		+ key 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};

function innerDeleteTheResponse( primaryKey, event, context, callback) {
	try {
		var actionName 	= "delete";
        callGet("TheResponse", actionName, primaryKey, event, callback );
	} catch ( err ) {
		console.log( "exception caught in innerDeleteTheResponse - " + getContextDetails(context, err) );
		throw err;
	}
};

// role related methods

 
 

/**
 * function to create the provided QuestionGroup data raising an exception if it fails
 */
exports.createQuestionGroup = function(event, context, callback) {	
	var businessObject = event.body;
		
	if ( businessObject == null ) {
        var errMsg = "error on createQuestionGroup - null QuestionGroup provided but not allowed. ";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
  
    try {
        innerCreateQuestionGroup( businessObject, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createQuestionGroup - unable to create QuestionGroup" + getContextDetails(context, err);
    	console.log( errMsg );
        callback( new Error( errMsg ) );
    }     
};

function innerCreateQuestionGroup( businessObject, event, context, callback ) {
	try {
        var actionName 		= "save";
		callPut("QuestionGroup", actionName, businessObject, event, callback);
	} catch (err) {
		console.log( "exception caught in innerCreateQuestionGroup - " + getContextDetails(context, err) );
		callback( err );
	}
};

/**
 * function to retrieve the QuestionGroup data via a supplied QuestionGroup primary key.
 */
exports.getQuestionGroup = function(event, context, callback) {
    var key = 	{ 
    				questionGroupId : 0
    			};                
	
	key.questionGroupId = event.questionGroupId;
    
    try {
        innerGetQuestionGroup( key, event, context, callback);        
    }
    catch( err ) {
        var errMsg = "err+or on createQuestionGroup - unable to locate QuestionGroup with key " 
        	+ key.toString() + " - " + getContextDetails(context, err);
    	console.log( errMsg );
        callback(  new Error( errMsg ) );
    }
};

function innerGetQuestionGroup( primaryKey, event, context, callback ) {
	try {
        var actionName	= "load";
		callGet("QuestionGroup", actionName, primaryKey, event, callback);
	} catch (err) {
		console.log( "exception caught in innerGetQuestionGroup - " + getContextDetails(context, err) );
		throw err;
	}
};

     
/**
 * function to save the provided QuestionGroup data.
 */
exports.saveQuestionGroup = function(event, context, callback) {
    var businessObject = event.body;                

	if ( !exists( businessObject ) ) {
        var errMsg = "error on createQuestionGroup - null QuestionGroup provided but not allowed.";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
	                
    if ( exists( businessObject.questionGroupId ) ) {
        try {                    
            innerSaveQuestionGroup(businessObject, event, context, callback);
        }
        catch (err) {
        	var errMsg = "error on createQuestionGroup - unable to save QuestionGroup " 
        		+ " - " + getContextDetails(context, err);
    		console.log( errMsg );
        	callback( new Error( errMsg ) );        
        }
    }
    else {
        var errMsg = "Unable to create QuestionGroup due to it having a null QuestionGroupPrimaryKey." 
        				+ " - " + getContextDetails(context)
        console.log( errMsg );
        callback( new Error( errMsg ) );
    }
    
};
     
function innerSaveQuestionGroup( businessObject, event, context, callback ) {
	try {
		var actionName	= "save";
        callPut("QuestionGroup", actionName, businessObject, event, callback);
	}
	catch ( err ) {
		console.log( "exception caught in innerSaveQuestionGroup - " + getContextDetails(context, err) );
		throw err;
	}
};

/**
 * function to retrieve a all QuestionGroups
 */
exports.getAllQuestionGroup = function(event, context, callback) {
    try {
        var actionName = "viewAll";
        innerGetAllQuestionGroup(event, context, callback);  
    } catch( err ) {
    	var errMsg = "error on createQuestionGroup - failed to getAllQuestionGroup " 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};
           
function innerGetAllQuestionGroup(event, context, callback) {
	try {
		var actionName = "viewAll";
        callGet("QuestionGroup", actionName, null, event, callback);	
	} catch( err ) {
		console.log( "exception caught in innerGetAllQuestionGroup - " + getContextDetails(context, err) );
	    throw err;
	}
}; 
     
/**
 * function to delete the associated QuestionGroup using its provided primary key.
 */
exports.deleteQuestionGroup = function(event, context, callback) {
    var key = 	{ 
    				questionGroupId : 0
    			};                
	
	key.questionGroupId = event.questionGroupId;
	
    try {                    
       	innerDeleteQuestionGroup(key, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createQuestionGroup - Unable to delete QuestionGroup using key = "  
    		+ key 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};

function innerDeleteQuestionGroup( primaryKey, event, context, callback) {
	try {
		var actionName 	= "delete";
        callGet("QuestionGroup", actionName, primaryKey, event, callback );
	} catch ( err ) {
		console.log( "exception caught in innerDeleteQuestionGroup - " + getContextDetails(context, err) );
		throw err;
	}
};

// role related methods

 
/**
 * function to retrieve the Questions on a QuestionGroup
 * @param parentKey
 */
exports.getQuestionsOnQuestionGroup = function(event, context, callback) {
    var keys = 	event.body;
    callGet("QuestionGroup", "loadQuestions", keys, event, callback);
};
    
/**
 * function to save multiple Question entities as the Questions 
 * of the relevant QuestionGroup associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.assignQuestionsOnQuestionGroup = function(event, context, callback) {
    var keys = 	event.body;
    callGet("QuestionGroup", "saveQuestions", keys, event, callback);
};

/**
 * function to delete multiple Question entities as the Questions 
 * of the relevant QuestionGroup associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.deleteQuestionsOnQuestionGroup = function(event, context, callback) {		
    var keys = 	event.body;
    callGet("QuestionGroup", "deleteQuestions", keys, event, callback);
};

/**
 * function to retrieve the QuestionGroups on a QuestionGroup
 * @param parentKey
 */
exports.getQuestionGroupsOnQuestionGroup = function(event, context, callback) {
    var keys = 	event.body;
    callGet("QuestionGroup", "loadQuestionGroups", keys, event, callback);
};
    
/**
 * function to save multiple QuestionGroup entities as the QuestionGroups 
 * of the relevant QuestionGroup associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.assignQuestionGroupsOnQuestionGroup = function(event, context, callback) {
    var keys = 	event.body;
    callGet("QuestionGroup", "saveQuestionGroups", keys, event, callback);
};

/**
 * function to delete multiple QuestionGroup entities as the QuestionGroups 
 * of the relevant QuestionGroup associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.deleteQuestionGroupsOnQuestionGroup = function(event, context, callback) {		
    var keys = 	event.body;
    callGet("QuestionGroup", "deleteQuestionGroups", keys, event, callback);
};

 

/**
 * function to create the provided Admin data raising an exception if it fails
 */
exports.createAdmin = function(event, context, callback) {	
	var businessObject = event.body;
		
	if ( businessObject == null ) {
        var errMsg = "error on createAdmin - null Admin provided but not allowed. ";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
  
    try {
        innerCreateAdmin( businessObject, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createAdmin - unable to create Admin" + getContextDetails(context, err);
    	console.log( errMsg );
        callback( new Error( errMsg ) );
    }     
};

function innerCreateAdmin( businessObject, event, context, callback ) {
	try {
        var actionName 		= "save";
		callPut("Admin", actionName, businessObject, event, callback);
	} catch (err) {
		console.log( "exception caught in innerCreateAdmin - " + getContextDetails(context, err) );
		callback( err );
	}
};

/**
 * function to retrieve the Admin data via a supplied Admin primary key.
 */
exports.getAdmin = function(event, context, callback) {
    var key = 	{ 
    				adminId : 0
    			};                
	
	key.adminId = event.adminId;
    
    try {
        innerGetAdmin( key, event, context, callback);        
    }
    catch( err ) {
        var errMsg = "err+or on createAdmin - unable to locate Admin with key " 
        	+ key.toString() + " - " + getContextDetails(context, err);
    	console.log( errMsg );
        callback(  new Error( errMsg ) );
    }
};

function innerGetAdmin( primaryKey, event, context, callback ) {
	try {
        var actionName	= "load";
		callGet("Admin", actionName, primaryKey, event, callback);
	} catch (err) {
		console.log( "exception caught in innerGetAdmin - " + getContextDetails(context, err) );
		throw err;
	}
};

     
/**
 * function to save the provided Admin data.
 */
exports.saveAdmin = function(event, context, callback) {
    var businessObject = event.body;                

	if ( !exists( businessObject ) ) {
        var errMsg = "error on createAdmin - null Admin provided but not allowed.";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
	                
    if ( exists( businessObject.adminId ) ) {
        try {                    
            innerSaveAdmin(businessObject, event, context, callback);
        }
        catch (err) {
        	var errMsg = "error on createAdmin - unable to save Admin " 
        		+ " - " + getContextDetails(context, err);
    		console.log( errMsg );
        	callback( new Error( errMsg ) );        
        }
    }
    else {
        var errMsg = "Unable to create Admin due to it having a null AdminPrimaryKey." 
        				+ " - " + getContextDetails(context)
        console.log( errMsg );
        callback( new Error( errMsg ) );
    }
    
};
     
function innerSaveAdmin( businessObject, event, context, callback ) {
	try {
		var actionName	= "save";
        callPut("Admin", actionName, businessObject, event, callback);
	}
	catch ( err ) {
		console.log( "exception caught in innerSaveAdmin - " + getContextDetails(context, err) );
		throw err;
	}
};

/**
 * function to retrieve a all Admins
 */
exports.getAllAdmin = function(event, context, callback) {
    try {
        var actionName = "viewAll";
        innerGetAllAdmin(event, context, callback);  
    } catch( err ) {
    	var errMsg = "error on createAdmin - failed to getAllAdmin " 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};
           
function innerGetAllAdmin(event, context, callback) {
	try {
		var actionName = "viewAll";
        callGet("Admin", actionName, null, event, callback);	
	} catch( err ) {
		console.log( "exception caught in innerGetAllAdmin - " + getContextDetails(context, err) );
	    throw err;
	}
}; 
     
/**
 * function to delete the associated Admin using its provided primary key.
 */
exports.deleteAdmin = function(event, context, callback) {
    var key = 	{ 
    				adminId : 0
    			};                
	
	key.adminId = event.adminId;
	
    try {                    
       	innerDeleteAdmin(key, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createAdmin - Unable to delete Admin using key = "  
    		+ key 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};

function innerDeleteAdmin( primaryKey, event, context, callback) {
	try {
		var actionName 	= "delete";
        callGet("Admin", actionName, primaryKey, event, callback );
	} catch ( err ) {
		console.log( "exception caught in innerDeleteAdmin - " + getContextDetails(context, err) );
		throw err;
	}
};

// role related methods

 
/**
 * function to retrieve the Users on a Admin
 * @param parentKey
 */
exports.getUsersOnAdmin = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Admin", "loadUsers", keys, event, callback);
};
    
/**
 * function to save multiple User entities as the Users 
 * of the relevant Admin associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.assignUsersOnAdmin = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Admin", "saveUsers", keys, event, callback);
};

/**
 * function to delete multiple User entities as the Users 
 * of the relevant Admin associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.deleteUsersOnAdmin = function(event, context, callback) {		
    var keys = 	event.body;
    callGet("Admin", "deleteUsers", keys, event, callback);
};

/**
 * function to retrieve the ReferenceEngines on a Admin
 * @param parentKey
 */
exports.getReferenceEnginesOnAdmin = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Admin", "loadReferenceEngines", keys, event, callback);
};
    
/**
 * function to save multiple ReferenceEngine entities as the ReferenceEngines 
 * of the relevant Admin associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.assignReferenceEnginesOnAdmin = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Admin", "saveReferenceEngines", keys, event, callback);
};

/**
 * function to delete multiple ReferenceEngine entities as the ReferenceEngines 
 * of the relevant Admin associated with the provided primary key
 * @param parentKey
 * @param childKeys
 */
exports.deleteReferenceEnginesOnAdmin = function(event, context, callback) {		
    var keys = 	event.body;
    callGet("Admin", "deleteReferenceEngines", keys, event, callback);
};

 

/**
 * function to create the provided Activity data raising an exception if it fails
 */
exports.createActivity = function(event, context, callback) {	
	var businessObject = event.body;
		
	if ( businessObject == null ) {
        var errMsg = "error on createActivity - null Activity provided but not allowed. ";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
  
    try {
        innerCreateActivity( businessObject, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createActivity - unable to create Activity" + getContextDetails(context, err);
    	console.log( errMsg );
        callback( new Error( errMsg ) );
    }     
};

function innerCreateActivity( businessObject, event, context, callback ) {
	try {
        var actionName 		= "save";
		callPut("Activity", actionName, businessObject, event, callback);
	} catch (err) {
		console.log( "exception caught in innerCreateActivity - " + getContextDetails(context, err) );
		callback( err );
	}
};

/**
 * function to retrieve the Activity data via a supplied Activity primary key.
 */
exports.getActivity = function(event, context, callback) {
    var key = 	{ 
    				activityId : 0
    			};                
	
	key.activityId = event.activityId;
    
    try {
        innerGetActivity( key, event, context, callback);        
    }
    catch( err ) {
        var errMsg = "err+or on createActivity - unable to locate Activity with key " 
        	+ key.toString() + " - " + getContextDetails(context, err);
    	console.log( errMsg );
        callback(  new Error( errMsg ) );
    }
};

function innerGetActivity( primaryKey, event, context, callback ) {
	try {
        var actionName	= "load";
		callGet("Activity", actionName, primaryKey, event, callback);
	} catch (err) {
		console.log( "exception caught in innerGetActivity - " + getContextDetails(context, err) );
		throw err;
	}
};

     
/**
 * function to save the provided Activity data.
 */
exports.saveActivity = function(event, context, callback) {
    var businessObject = event.body;                

	if ( !exists( businessObject ) ) {
        var errMsg = "error on createActivity - null Activity provided but not allowed.";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
	                
    if ( exists( businessObject.activityId ) ) {
        try {                    
            innerSaveActivity(businessObject, event, context, callback);
        }
        catch (err) {
        	var errMsg = "error on createActivity - unable to save Activity " 
        		+ " - " + getContextDetails(context, err);
    		console.log( errMsg );
        	callback( new Error( errMsg ) );        
        }
    }
    else {
        var errMsg = "Unable to create Activity due to it having a null ActivityPrimaryKey." 
        				+ " - " + getContextDetails(context)
        console.log( errMsg );
        callback( new Error( errMsg ) );
    }
    
};
     
function innerSaveActivity( businessObject, event, context, callback ) {
	try {
		var actionName	= "save";
        callPut("Activity", actionName, businessObject, event, callback);
	}
	catch ( err ) {
		console.log( "exception caught in innerSaveActivity - " + getContextDetails(context, err) );
		throw err;
	}
};

/**
 * function to retrieve a all Activitys
 */
exports.getAllActivity = function(event, context, callback) {
    try {
        var actionName = "viewAll";
        innerGetAllActivity(event, context, callback);  
    } catch( err ) {
    	var errMsg = "error on createActivity - failed to getAllActivity " 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};
           
function innerGetAllActivity(event, context, callback) {
	try {
		var actionName = "viewAll";
        callGet("Activity", actionName, null, event, callback);	
	} catch( err ) {
		console.log( "exception caught in innerGetAllActivity - " + getContextDetails(context, err) );
	    throw err;
	}
}; 
     
/**
 * function to delete the associated Activity using its provided primary key.
 */
exports.deleteActivity = function(event, context, callback) {
    var key = 	{ 
    				activityId : 0
    			};                
	
	key.activityId = event.activityId;
	
    try {                    
       	innerDeleteActivity(key, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createActivity - Unable to delete Activity using key = "  
    		+ key 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};

function innerDeleteActivity( primaryKey, event, context, callback) {
	try {
		var actionName 	= "delete";
        callGet("Activity", actionName, primaryKey, event, callback );
	} catch ( err ) {
		console.log( "exception caught in innerDeleteActivity - " + getContextDetails(context, err) );
		throw err;
	}
};

// role related methods

/**
 * function to get the User using the provided primary key of a Activity
 */
exports.getUserOnActivity = function(event, context, callback) {	
    var keys = 	event.body;
    callGet("Activity", "loadUser", keys, event, callback );
};

/**
 * function to assign the User on a Activity using the provided primary key of a User
 */
exports.saveUserOnActivity = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Activity", "saveUser", keys, event, callback);
};

/**
 * function to remove the assignment of the User on a Activity
 */
exports.deleteUserOnActivity = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Activity", "deleteUser", keys, event, callback);
};
		
 
 

/**
 * function to create the provided Comment data raising an exception if it fails
 */
exports.createComment = function(event, context, callback) {	
	var businessObject = event.body;
		
	if ( businessObject == null ) {
        var errMsg = "error on createComment - null Comment provided but not allowed. ";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
  
    try {
        innerCreateComment( businessObject, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createComment - unable to create Comment" + getContextDetails(context, err);
    	console.log( errMsg );
        callback( new Error( errMsg ) );
    }     
};

function innerCreateComment( businessObject, event, context, callback ) {
	try {
        var actionName 		= "save";
		callPut("Comment", actionName, businessObject, event, callback);
	} catch (err) {
		console.log( "exception caught in innerCreateComment - " + getContextDetails(context, err) );
		callback( err );
	}
};

/**
 * function to retrieve the Comment data via a supplied Comment primary key.
 */
exports.getComment = function(event, context, callback) {
    var key = 	{ 
    				commentId : 0
    			};                
	
	key.commentId = event.commentId;
    
    try {
        innerGetComment( key, event, context, callback);        
    }
    catch( err ) {
        var errMsg = "err+or on createComment - unable to locate Comment with key " 
        	+ key.toString() + " - " + getContextDetails(context, err);
    	console.log( errMsg );
        callback(  new Error( errMsg ) );
    }
};

function innerGetComment( primaryKey, event, context, callback ) {
	try {
        var actionName	= "load";
		callGet("Comment", actionName, primaryKey, event, callback);
	} catch (err) {
		console.log( "exception caught in innerGetComment - " + getContextDetails(context, err) );
		throw err;
	}
};

     
/**
 * function to save the provided Comment data.
 */
exports.saveComment = function(event, context, callback) {
    var businessObject = event.body;                

	if ( !exists( businessObject ) ) {
        var errMsg = "error on createComment - null Comment provided but not allowed.";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
	                
    if ( exists( businessObject.commentId ) ) {
        try {                    
            innerSaveComment(businessObject, event, context, callback);
        }
        catch (err) {
        	var errMsg = "error on createComment - unable to save Comment " 
        		+ " - " + getContextDetails(context, err);
    		console.log( errMsg );
        	callback( new Error( errMsg ) );        
        }
    }
    else {
        var errMsg = "Unable to create Comment due to it having a null CommentPrimaryKey." 
        				+ " - " + getContextDetails(context)
        console.log( errMsg );
        callback( new Error( errMsg ) );
    }
    
};
     
function innerSaveComment( businessObject, event, context, callback ) {
	try {
		var actionName	= "save";
        callPut("Comment", actionName, businessObject, event, callback);
	}
	catch ( err ) {
		console.log( "exception caught in innerSaveComment - " + getContextDetails(context, err) );
		throw err;
	}
};

/**
 * function to retrieve a all Comments
 */
exports.getAllComment = function(event, context, callback) {
    try {
        var actionName = "viewAll";
        innerGetAllComment(event, context, callback);  
    } catch( err ) {
    	var errMsg = "error on createComment - failed to getAllComment " 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};
           
function innerGetAllComment(event, context, callback) {
	try {
		var actionName = "viewAll";
        callGet("Comment", actionName, null, event, callback);	
	} catch( err ) {
		console.log( "exception caught in innerGetAllComment - " + getContextDetails(context, err) );
	    throw err;
	}
}; 
     
/**
 * function to delete the associated Comment using its provided primary key.
 */
exports.deleteComment = function(event, context, callback) {
    var key = 	{ 
    				commentId : 0
    			};                
	
	key.commentId = event.commentId;
	
    try {                    
       	innerDeleteComment(key, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createComment - Unable to delete Comment using key = "  
    		+ key 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};

function innerDeleteComment( primaryKey, event, context, callback) {
	try {
		var actionName 	= "delete";
        callGet("Comment", actionName, primaryKey, event, callback );
	} catch ( err ) {
		console.log( "exception caught in innerDeleteComment - " + getContextDetails(context, err) );
		throw err;
	}
};

// role related methods

/**
 * function to get the Source using the provided primary key of a Comment
 */
exports.getSourceOnComment = function(event, context, callback) {	
    var keys = 	event.body;
    callGet("Comment", "loadSource", keys, event, callback );
};

/**
 * function to assign the Source on a Comment using the provided primary key of a Referrer
 */
exports.saveSourceOnComment = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Comment", "saveSource", keys, event, callback);
};

/**
 * function to remove the assignment of the Source on a Comment
 */
exports.deleteSourceOnComment = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Comment", "deleteSource", keys, event, callback);
};
		
 
 

/**
 * function to create the provided Answer data raising an exception if it fails
 */
exports.createAnswer = function(event, context, callback) {	
	var businessObject = event.body;
		
	if ( businessObject == null ) {
        var errMsg = "error on createAnswer - null Answer provided but not allowed. ";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
  
    try {
        innerCreateAnswer( businessObject, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createAnswer - unable to create Answer" + getContextDetails(context, err);
    	console.log( errMsg );
        callback( new Error( errMsg ) );
    }     
};

function innerCreateAnswer( businessObject, event, context, callback ) {
	try {
        var actionName 		= "save";
		callPut("Answer", actionName, businessObject, event, callback);
	} catch (err) {
		console.log( "exception caught in innerCreateAnswer - " + getContextDetails(context, err) );
		callback( err );
	}
};

/**
 * function to retrieve the Answer data via a supplied Answer primary key.
 */
exports.getAnswer = function(event, context, callback) {
    var key = 	{ 
    				answerId : 0
    			};                
	
	key.answerId = event.answerId;
    
    try {
        innerGetAnswer( key, event, context, callback);        
    }
    catch( err ) {
        var errMsg = "err+or on createAnswer - unable to locate Answer with key " 
        	+ key.toString() + " - " + getContextDetails(context, err);
    	console.log( errMsg );
        callback(  new Error( errMsg ) );
    }
};

function innerGetAnswer( primaryKey, event, context, callback ) {
	try {
        var actionName	= "load";
		callGet("Answer", actionName, primaryKey, event, callback);
	} catch (err) {
		console.log( "exception caught in innerGetAnswer - " + getContextDetails(context, err) );
		throw err;
	}
};

     
/**
 * function to save the provided Answer data.
 */
exports.saveAnswer = function(event, context, callback) {
    var businessObject = event.body;                

	if ( !exists( businessObject ) ) {
        var errMsg = "error on createAnswer - null Answer provided but not allowed.";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
	                
    if ( exists( businessObject.answerId ) ) {
        try {                    
            innerSaveAnswer(businessObject, event, context, callback);
        }
        catch (err) {
        	var errMsg = "error on createAnswer - unable to save Answer " 
        		+ " - " + getContextDetails(context, err);
    		console.log( errMsg );
        	callback( new Error( errMsg ) );        
        }
    }
    else {
        var errMsg = "Unable to create Answer due to it having a null AnswerPrimaryKey." 
        				+ " - " + getContextDetails(context)
        console.log( errMsg );
        callback( new Error( errMsg ) );
    }
    
};
     
function innerSaveAnswer( businessObject, event, context, callback ) {
	try {
		var actionName	= "save";
        callPut("Answer", actionName, businessObject, event, callback);
	}
	catch ( err ) {
		console.log( "exception caught in innerSaveAnswer - " + getContextDetails(context, err) );
		throw err;
	}
};

/**
 * function to retrieve a all Answers
 */
exports.getAllAnswer = function(event, context, callback) {
    try {
        var actionName = "viewAll";
        innerGetAllAnswer(event, context, callback);  
    } catch( err ) {
    	var errMsg = "error on createAnswer - failed to getAllAnswer " 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};
           
function innerGetAllAnswer(event, context, callback) {
	try {
		var actionName = "viewAll";
        callGet("Answer", actionName, null, event, callback);	
	} catch( err ) {
		console.log( "exception caught in innerGetAllAnswer - " + getContextDetails(context, err) );
	    throw err;
	}
}; 
     
/**
 * function to delete the associated Answer using its provided primary key.
 */
exports.deleteAnswer = function(event, context, callback) {
    var key = 	{ 
    				answerId : 0
    			};                
	
	key.answerId = event.answerId;
	
    try {                    
       	innerDeleteAnswer(key, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createAnswer - Unable to delete Answer using key = "  
    		+ key 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};

function innerDeleteAnswer( primaryKey, event, context, callback) {
	try {
		var actionName 	= "delete";
        callGet("Answer", actionName, primaryKey, event, callback );
	} catch ( err ) {
		console.log( "exception caught in innerDeleteAnswer - " + getContextDetails(context, err) );
		throw err;
	}
};

// role related methods

/**
 * function to get the Question using the provided primary key of a Answer
 */
exports.getQuestionOnAnswer = function(event, context, callback) {	
    var keys = 	event.body;
    callGet("Answer", "loadQuestion", keys, event, callback );
};

/**
 * function to assign the Question on a Answer using the provided primary key of a Question
 */
exports.saveQuestionOnAnswer = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Answer", "saveQuestion", keys, event, callback);
};

/**
 * function to remove the assignment of the Question on a Answer
 */
exports.deleteQuestionOnAnswer = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Answer", "deleteQuestion", keys, event, callback);
};
		
/**
 * function to get the Response using the provided primary key of a Answer
 */
exports.getResponseOnAnswer = function(event, context, callback) {	
    var keys = 	event.body;
    callGet("Answer", "loadResponse", keys, event, callback );
};

/**
 * function to assign the Response on a Answer using the provided primary key of a TheResponse
 */
exports.saveResponseOnAnswer = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Answer", "saveResponse", keys, event, callback);
};

/**
 * function to remove the assignment of the Response on a Answer
 */
exports.deleteResponseOnAnswer = function(event, context, callback) {
    var keys = 	event.body;
    callGet("Answer", "deleteResponse", keys, event, callback);
};
		
 
 

/**
 * function to create the provided ReferenceGroupLink data raising an exception if it fails
 */
exports.createReferenceGroupLink = function(event, context, callback) {	
	var businessObject = event.body;
		
	if ( businessObject == null ) {
        var errMsg = "error on createReferenceGroupLink - null ReferenceGroupLink provided but not allowed. ";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
  
    try {
        innerCreateReferenceGroupLink( businessObject, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createReferenceGroupLink - unable to create ReferenceGroupLink" + getContextDetails(context, err);
    	console.log( errMsg );
        callback( new Error( errMsg ) );
    }     
};

function innerCreateReferenceGroupLink( businessObject, event, context, callback ) {
	try {
        var actionName 		= "save";
		callPut("ReferenceGroupLink", actionName, businessObject, event, callback);
	} catch (err) {
		console.log( "exception caught in innerCreateReferenceGroupLink - " + getContextDetails(context, err) );
		callback( err );
	}
};

/**
 * function to retrieve the ReferenceGroupLink data via a supplied ReferenceGroupLink primary key.
 */
exports.getReferenceGroupLink = function(event, context, callback) {
    var key = 	{ 
    				referenceGroupLinkId : 0
    			};                
	
	key.referenceGroupLinkId = event.referenceGroupLinkId;
    
    try {
        innerGetReferenceGroupLink( key, event, context, callback);        
    }
    catch( err ) {
        var errMsg = "err+or on createReferenceGroupLink - unable to locate ReferenceGroupLink with key " 
        	+ key.toString() + " - " + getContextDetails(context, err);
    	console.log( errMsg );
        callback(  new Error( errMsg ) );
    }
};

function innerGetReferenceGroupLink( primaryKey, event, context, callback ) {
	try {
        var actionName	= "load";
		callGet("ReferenceGroupLink", actionName, primaryKey, event, callback);
	} catch (err) {
		console.log( "exception caught in innerGetReferenceGroupLink - " + getContextDetails(context, err) );
		throw err;
	}
};

     
/**
 * function to save the provided ReferenceGroupLink data.
 */
exports.saveReferenceGroupLink = function(event, context, callback) {
    var businessObject = event.body;                

	if ( !exists( businessObject ) ) {
        var errMsg = "error on createReferenceGroupLink - null ReferenceGroupLink provided but not allowed.";
        console.log( errMsg );
        callback( new Error( errMsg ) ); 
    }
	                
    if ( exists( businessObject.referenceGroupLinkId ) ) {
        try {                    
            innerSaveReferenceGroupLink(businessObject, event, context, callback);
        }
        catch (err) {
        	var errMsg = "error on createReferenceGroupLink - unable to save ReferenceGroupLink " 
        		+ " - " + getContextDetails(context, err);
    		console.log( errMsg );
        	callback( new Error( errMsg ) );        
        }
    }
    else {
        var errMsg = "Unable to create ReferenceGroupLink due to it having a null ReferenceGroupLinkPrimaryKey." 
        				+ " - " + getContextDetails(context)
        console.log( errMsg );
        callback( new Error( errMsg ) );
    }
    
};
     
function innerSaveReferenceGroupLink( businessObject, event, context, callback ) {
	try {
		var actionName	= "save";
        callPut("ReferenceGroupLink", actionName, businessObject, event, callback);
	}
	catch ( err ) {
		console.log( "exception caught in innerSaveReferenceGroupLink - " + getContextDetails(context, err) );
		throw err;
	}
};

/**
 * function to retrieve a all ReferenceGroupLinks
 */
exports.getAllReferenceGroupLink = function(event, context, callback) {
    try {
        var actionName = "viewAll";
        innerGetAllReferenceGroupLink(event, context, callback);  
    } catch( err ) {
    	var errMsg = "error on createReferenceGroupLink - failed to getAllReferenceGroupLink " 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};
           
function innerGetAllReferenceGroupLink(event, context, callback) {
	try {
		var actionName = "viewAll";
        callGet("ReferenceGroupLink", actionName, null, event, callback);	
	} catch( err ) {
		console.log( "exception caught in innerGetAllReferenceGroupLink - " + getContextDetails(context, err) );
	    throw err;
	}
}; 
     
/**
 * function to delete the associated ReferenceGroupLink using its provided primary key.
 */
exports.deleteReferenceGroupLink = function(event, context, callback) {
    var key = 	{ 
    				referenceGroupLinkId : 0
    			};                
	
	key.referenceGroupLinkId = event.referenceGroupLinkId;
	
    try {                    
       	innerDeleteReferenceGroupLink(key, event, context, callback);
    } catch (err) {
    	var errMsg = "error on createReferenceGroupLink - Unable to delete ReferenceGroupLink using key = "  
    		+ key 
    		+ " - " + getContextDetails(context, err);
		console.log( errMsg );
    	callback( new Error( errMsg ) ); 
    }
};

function innerDeleteReferenceGroupLink( primaryKey, event, context, callback) {
	try {
		var actionName 	= "delete";
        callGet("ReferenceGroupLink", actionName, primaryKey, event, callback );
	} catch ( err ) {
		console.log( "exception caught in innerDeleteReferenceGroupLink - " + getContextDetails(context, err) );
		throw err;
	}
};

// role related methods

/**
 * function to get the ReferrerGroup using the provided primary key of a ReferenceGroupLink
 */
exports.getReferrerGroupOnReferenceGroupLink = function(event, context, callback) {	
    var keys = 	event.body;
    callGet("ReferenceGroupLink", "loadReferrerGroup", keys, event, callback );
};

/**
 * function to assign the ReferrerGroup on a ReferenceGroupLink using the provided primary key of a ReferrerGroup
 */
exports.saveReferrerGroupOnReferenceGroupLink = function(event, context, callback) {
    var keys = 	event.body;
    callGet("ReferenceGroupLink", "saveReferrerGroup", keys, event, callback);
};

/**
 * function to remove the assignment of the ReferrerGroup on a ReferenceGroupLink
 */
exports.deleteReferrerGroupOnReferenceGroupLink = function(event, context, callback) {
    var keys = 	event.body;
    callGet("ReferenceGroupLink", "deleteReferrerGroup", keys, event, callback);
};
		
/**
 * function to get the LinkProvider using the provided primary key of a ReferenceGroupLink
 */
exports.getLinkProviderOnReferenceGroupLink = function(event, context, callback) {	
    var keys = 	event.body;
    callGet("ReferenceGroupLink", "loadLinkProvider", keys, event, callback );
};

/**
 * function to assign the LinkProvider on a ReferenceGroupLink using the provided primary key of a User
 */
exports.saveLinkProviderOnReferenceGroupLink = function(event, context, callback) {
    var keys = 	event.body;
    callGet("ReferenceGroupLink", "saveLinkProvider", keys, event, callback);
};

/**
 * function to remove the assignment of the LinkProvider on a ReferenceGroupLink
 */
exports.deleteLinkProviderOnReferenceGroupLink = function(event, context, callback) {
    var keys = 	event.body;
    callGet("ReferenceGroupLink", "deleteLinkProvider", keys, event, callback);
};
		
 
 
 		
/**
 * internal function used to handle HTTP request to the RESTful API
 */		
function callGet( packageName, actionName, data, event, callback ) {
	return( call( packageName, actionName, data, event, callback,  'GET' ) );
};

function callPut( packageName, actionName, data, event, callback ) {
	return( call( packageName, actionName, data, event, callback, 'PUT' ) );
};

function call( packageName, actionName, data, event, callback, method) {
	var querystring	= require('querystring');
	var https 		= require('http');
	var host 		= RESTFUL_API_DAO_URL;
	var endPoint 	= "/" + packageName + "/" + actionName;
  	var dataString 	= JSON.stringify(data);
	var headers 	= {};
	
	console.log("datastring is " + dataString );
	
	if (method == 'GET') {
		endPoint += '?' + querystring.stringify(data);
	}
	else {
		endPoint += '?' + dataString;	
		headers = {
			'Content-Type': 'application/json',
			'Content-Length': dataString.length
		};
	}
		
  	var options = {
  		hostname: host,
    	path: endPoint,
    	method: "GET",
    	port: PORT,
    	headers: headers
  	};

  	console.log( options );
	var responseString = '';
	var req = https.request(options, function(res) {
    	res.setEncoding('utf-8');
    	res.on('data', function(data) {
      		responseString += data;
    	});
       	res.on('end', function() {    
	    	callback(null, JSON.parse(responseString));	
     	});
	});
	
	req.write( dataString );
	req.end();
  		
};

function getContextDetails( context, error ) {
	var errMsg = '';
	if ( exists( context ) ) {
		errMsg = errMsg + 'remaining time = ' + context.getRemainingTimeInMillis()
	    				+ ', functionName = ' +  context.functionName
	    				+ ', AWSrequestID = ' + context.awsRequestId
	    				+ ', logGroupName = ' + context.log_group_name
	    				+ ', logStreamName = ' + context.log_stream_name
	    				+ ', clientContext = ' + context.clientContext;
	    if ( exists( context.identity ) ) 
	        errMsg = errMsg + ', Cognito identity ID = ' + context.identity.cognitoIdentityId;
	}
		        	
	if ( exits( error ) )
		errMsg = errMsg + ', ' + error.message;
		
    return( errMsg );
};

function exists( objToTest ) {
	if (null == objToTest) 
		return false;
  	if ("undefined" == typeof(objToTest)) 
  		return false;
  	return true;
};

// global vars				   
var errHandler = function(err) {
    console.log(err);
};

var RESTFUL_API_DAO_URL 	= process.env.delegateDAOHost;
var PORT 					= process.env.delegateDAOPort;
var KINESIS_STREAM_NAME		= process.env.kinesisStreamName;