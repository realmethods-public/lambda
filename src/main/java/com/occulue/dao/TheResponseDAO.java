/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.dao;

import com.occulue.bo.*;

import com.occulue.dao.*;

import com.occulue.exception.ProcessingException;

import com.occulue.primarykey.*;

import org.hibernate.*;

import org.hibernate.cfg.*;

import java.sql.*;

import java.util.*;
import java.util.logging.Logger;


/**
 * Implements the Hibernate persistence processing for business entity TheResponse.
 *
 * @author dev@realmethods.com
 */

// AIB : #getDAOClassDecl()
public class TheResponseDAO extends BaseDAO // ~AIB
 {
    // AIB : #outputDAOFindAllImplementations()
    // ~AIB

    //*****************************************************
    // Attributes
    //*****************************************************
    private static final Logger LOGGER = Logger.getLogger(TheResponse.class.getName());

    /**
     * default constructor
     */
    public TheResponseDAO() {
    }

    /**
     * Retrieves a TheResponse from the persistent store, using the provided primary key.
     * If no match is found, a null TheResponse is returned.
     * <p>
     * @param       pk
     * @return      TheResponse
     * @exception   ProcessingException
     */
    public TheResponse findTheResponse(TheResponsePrimaryKey pk)
        throws ProcessingException {
        if (pk == null) {
            throw new ProcessingException(
                "TheResponseDAO.findTheResponse(...) cannot have a null primary key argument");
        }

        Query query = null;
        TheResponse businessObject = null;

        StringBuilder fromClause = new StringBuilder(
                "from com.occulue.bo.TheResponse as theresponse where ");

        Session session = null;
        Transaction tx = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            // AIB : #getHibernateFindFromClause()
            fromClause.append("theresponse.theResponseId = " +
                pk.getTheResponseId().toString());
            // ~AIB
            query = session.createQuery(fromClause.toString());

            if (query != null) {
                businessObject = new TheResponse();
                businessObject.copy((TheResponse) query.list().iterator().next());
            }

            commitTransaction(tx);
        } catch (Throwable exc) {
            businessObject = null;
            exc.printStackTrace();
            throw new ProcessingException(
                "TheResponseDAO.findTheResponse failed for primary key " + pk +
                " - " + exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "TheResponseDAO.findTheResponse - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
     * returns a Collection of all TheResponses
     * @return                ArrayList<TheResponse>
     * @exception   ProcessingException
     */
    public ArrayList<TheResponse> findAllTheResponse()
        throws ProcessingException {
        ArrayList<TheResponse> list = new ArrayList<TheResponse>();
        ArrayList<TheResponse> refList = new ArrayList<TheResponse>();
        Query query = null;
        StringBuilder buf = new StringBuilder("from com.occulue.bo.TheResponse");

        try {
            Session session = currentSession();

            query = session.createQuery(buf.toString());

            if (query != null) {
                list = (ArrayList<TheResponse>) query.list();

                TheResponse tmp = null;

                for (TheResponse listEntry : list) {
                    tmp = new TheResponse();
                    tmp.copyShallow(listEntry);
                    refList.add(tmp);
                }
            }
        } catch (Throwable exc) {
            exc.printStackTrace();
            throw new ProcessingException(
                "TheResponseDAO.findAllTheResponse failed - " + exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "TheResponseDAO.findAllTheResponse - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        if (list.size() <= 0) {
            LOGGER.info("TheResponseDAO:findAllTheResponses() - List is empty.");
        }

        return (refList);
    }

    /**
     * Inserts a new TheResponse into the persistent store.
     * @param       businessObject
     * @return      newly persisted TheResponse
     * @exception   ProcessingException
     */
    public TheResponse createTheResponse(TheResponse businessObject)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.save(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "TheResponseDAO.createTheResponse - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "TheResponseDAO.createTheResponse failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "TheResponseDAO.createTheResponse - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        // return the businessObject
        return (businessObject);
    }

    /**
     * Stores the provided TheResponse to the persistent store.
     *
     * @param       businessObject
     * @return      TheResponse        stored entity
     * @exception   ProcessingException
     */
    public TheResponse saveTheResponse(TheResponse businessObject)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.update(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "TheResponseDAO.saveTheResponse - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "TheResponseDAO.saveTheResponse failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "TheResponseDAO.saveTheResponse - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
    * Removes a TheResponse from the persistent store.
    *
    * @param        pk                identity of object to remove
    * @exception    ProcessingException
    */
    public void deleteTheResponse(TheResponsePrimaryKey pk)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            TheResponse bo = findTheResponse(pk);

            session = currentSession();
            tx = currentTransaction(session);
            session.delete(bo);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "TheResponseDAO.deleteTheResponse - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "TheResponseDAO.deleteTheResponse failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "TheResponseDAO.deleteTheResponse - Hibernate failed to close the Session - " +
                    exc);
            }
        }
    }
}
