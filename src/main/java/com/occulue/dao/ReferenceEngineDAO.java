/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.dao;

import com.occulue.bo.*;

import com.occulue.dao.*;

import com.occulue.exception.ProcessingException;

import com.occulue.primarykey.*;

import org.hibernate.*;

import org.hibernate.cfg.*;

import java.sql.*;

import java.util.*;
import java.util.logging.Logger;


/**
 * Implements the Hibernate persistence processing for business entity ReferenceEngine.
 *
 * @author dev@realmethods.com
 */

// AIB : #getDAOClassDecl()
public class ReferenceEngineDAO extends BaseDAO // ~AIB
 {
    // AIB : #outputDAOFindAllImplementations()
    // ~AIB

    //*****************************************************
    // Attributes
    //*****************************************************
    private static final Logger LOGGER = Logger.getLogger(ReferenceEngine.class.getName());

    /**
     * default constructor
     */
    public ReferenceEngineDAO() {
    }

    /**
     * Retrieves a ReferenceEngine from the persistent store, using the provided primary key.
     * If no match is found, a null ReferenceEngine is returned.
     * <p>
     * @param       pk
     * @return      ReferenceEngine
     * @exception   ProcessingException
     */
    public ReferenceEngine findReferenceEngine(ReferenceEnginePrimaryKey pk)
        throws ProcessingException {
        if (pk == null) {
            throw new ProcessingException(
                "ReferenceEngineDAO.findReferenceEngine(...) cannot have a null primary key argument");
        }

        Query query = null;
        ReferenceEngine businessObject = null;

        StringBuilder fromClause = new StringBuilder(
                "from com.occulue.bo.ReferenceEngine as referenceengine where ");

        Session session = null;
        Transaction tx = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            // AIB : #getHibernateFindFromClause()
            fromClause.append("referenceengine.referenceEngineId = " +
                pk.getReferenceEngineId().toString());
            // ~AIB
            query = session.createQuery(fromClause.toString());

            if (query != null) {
                businessObject = new ReferenceEngine();
                businessObject.copy((ReferenceEngine) query.list().iterator()
                                                           .next());
            }

            commitTransaction(tx);
        } catch (Throwable exc) {
            businessObject = null;
            exc.printStackTrace();
            throw new ProcessingException(
                "ReferenceEngineDAO.findReferenceEngine failed for primary key " +
                pk + " - " + exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferenceEngineDAO.findReferenceEngine - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
     * returns a Collection of all ReferenceEngines
     * @return                ArrayList<ReferenceEngine>
     * @exception   ProcessingException
     */
    public ArrayList<ReferenceEngine> findAllReferenceEngine()
        throws ProcessingException {
        ArrayList<ReferenceEngine> list = new ArrayList<ReferenceEngine>();
        ArrayList<ReferenceEngine> refList = new ArrayList<ReferenceEngine>();
        Query query = null;
        StringBuilder buf = new StringBuilder(
                "from com.occulue.bo.ReferenceEngine");

        try {
            Session session = currentSession();

            query = session.createQuery(buf.toString());

            if (query != null) {
                list = (ArrayList<ReferenceEngine>) query.list();

                ReferenceEngine tmp = null;

                for (ReferenceEngine listEntry : list) {
                    tmp = new ReferenceEngine();
                    tmp.copyShallow(listEntry);
                    refList.add(tmp);
                }
            }
        } catch (Throwable exc) {
            exc.printStackTrace();
            throw new ProcessingException(
                "ReferenceEngineDAO.findAllReferenceEngine failed - " + exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferenceEngineDAO.findAllReferenceEngine - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        if (list.size() <= 0) {
            LOGGER.info(
                "ReferenceEngineDAO:findAllReferenceEngines() - List is empty.");
        }

        return (refList);
    }

    /**
     * Inserts a new ReferenceEngine into the persistent store.
     * @param       businessObject
     * @return      newly persisted ReferenceEngine
     * @exception   ProcessingException
     */
    public ReferenceEngine createReferenceEngine(ReferenceEngine businessObject)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.save(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "ReferenceEngineDAO.createReferenceEngine - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "ReferenceEngineDAO.createReferenceEngine failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferenceEngineDAO.createReferenceEngine - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        // return the businessObject
        return (businessObject);
    }

    /**
     * Stores the provided ReferenceEngine to the persistent store.
     *
     * @param       businessObject
     * @return      ReferenceEngine        stored entity
     * @exception   ProcessingException
     */
    public ReferenceEngine saveReferenceEngine(ReferenceEngine businessObject)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.update(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "ReferenceEngineDAO.saveReferenceEngine - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "ReferenceEngineDAO.saveReferenceEngine failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferenceEngineDAO.saveReferenceEngine - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
    * Removes a ReferenceEngine from the persistent store.
    *
    * @param        pk                identity of object to remove
    * @exception    ProcessingException
    */
    public void deleteReferenceEngine(ReferenceEnginePrimaryKey pk)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            ReferenceEngine bo = findReferenceEngine(pk);

            session = currentSession();
            tx = currentTransaction(session);
            session.delete(bo);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "ReferenceEngineDAO.deleteReferenceEngine - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "ReferenceEngineDAO.deleteReferenceEngine failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferenceEngineDAO.deleteReferenceEngine - Hibernate failed to close the Session - " +
                    exc);
            }
        }
    }
}
