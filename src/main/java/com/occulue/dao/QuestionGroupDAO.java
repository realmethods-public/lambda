/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.dao;

import com.occulue.bo.*;

import com.occulue.dao.*;

import com.occulue.exception.ProcessingException;

import com.occulue.primarykey.*;

import org.hibernate.*;

import org.hibernate.cfg.*;

import java.sql.*;

import java.util.*;
import java.util.logging.Logger;


/**
 * Implements the Hibernate persistence processing for business entity QuestionGroup.
 *
 * @author dev@realmethods.com
 */

// AIB : #getDAOClassDecl()
public class QuestionGroupDAO extends BaseDAO // ~AIB
 {
    // AIB : #outputDAOFindAllImplementations()
    // ~AIB

    //*****************************************************
    // Attributes
    //*****************************************************
    private static final Logger LOGGER = Logger.getLogger(QuestionGroup.class.getName());

    /**
     * default constructor
     */
    public QuestionGroupDAO() {
    }

    /**
     * Retrieves a QuestionGroup from the persistent store, using the provided primary key.
     * If no match is found, a null QuestionGroup is returned.
     * <p>
     * @param       pk
     * @return      QuestionGroup
     * @exception   ProcessingException
     */
    public QuestionGroup findQuestionGroup(QuestionGroupPrimaryKey pk)
        throws ProcessingException {
        if (pk == null) {
            throw new ProcessingException(
                "QuestionGroupDAO.findQuestionGroup(...) cannot have a null primary key argument");
        }

        Query query = null;
        QuestionGroup businessObject = null;

        StringBuilder fromClause = new StringBuilder(
                "from com.occulue.bo.QuestionGroup as questiongroup where ");

        Session session = null;
        Transaction tx = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            // AIB : #getHibernateFindFromClause()
            fromClause.append("questiongroup.questionGroupId = " +
                pk.getQuestionGroupId().toString());
            // ~AIB
            query = session.createQuery(fromClause.toString());

            if (query != null) {
                businessObject = new QuestionGroup();
                businessObject.copy((QuestionGroup) query.list().iterator()
                                                         .next());
            }

            commitTransaction(tx);
        } catch (Throwable exc) {
            businessObject = null;
            exc.printStackTrace();
            throw new ProcessingException(
                "QuestionGroupDAO.findQuestionGroup failed for primary key " +
                pk + " - " + exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "QuestionGroupDAO.findQuestionGroup - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
     * returns a Collection of all QuestionGroups
     * @return                ArrayList<QuestionGroup>
     * @exception   ProcessingException
     */
    public ArrayList<QuestionGroup> findAllQuestionGroup()
        throws ProcessingException {
        ArrayList<QuestionGroup> list = new ArrayList<QuestionGroup>();
        ArrayList<QuestionGroup> refList = new ArrayList<QuestionGroup>();
        Query query = null;
        StringBuilder buf = new StringBuilder(
                "from com.occulue.bo.QuestionGroup");

        try {
            Session session = currentSession();

            query = session.createQuery(buf.toString());

            if (query != null) {
                list = (ArrayList<QuestionGroup>) query.list();

                QuestionGroup tmp = null;

                for (QuestionGroup listEntry : list) {
                    tmp = new QuestionGroup();
                    tmp.copyShallow(listEntry);
                    refList.add(tmp);
                }
            }
        } catch (Throwable exc) {
            exc.printStackTrace();
            throw new ProcessingException(
                "QuestionGroupDAO.findAllQuestionGroup failed - " + exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "QuestionGroupDAO.findAllQuestionGroup - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        if (list.size() <= 0) {
            LOGGER.info(
                "QuestionGroupDAO:findAllQuestionGroups() - List is empty.");
        }

        return (refList);
    }

    /**
     * Inserts a new QuestionGroup into the persistent store.
     * @param       businessObject
     * @return      newly persisted QuestionGroup
     * @exception   ProcessingException
     */
    public QuestionGroup createQuestionGroup(QuestionGroup businessObject)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.save(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "QuestionGroupDAO.createQuestionGroup - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "QuestionGroupDAO.createQuestionGroup failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "QuestionGroupDAO.createQuestionGroup - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        // return the businessObject
        return (businessObject);
    }

    /**
     * Stores the provided QuestionGroup to the persistent store.
     *
     * @param       businessObject
     * @return      QuestionGroup        stored entity
     * @exception   ProcessingException
     */
    public QuestionGroup saveQuestionGroup(QuestionGroup businessObject)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.update(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "QuestionGroupDAO.saveQuestionGroup - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "QuestionGroupDAO.saveQuestionGroup failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "QuestionGroupDAO.saveQuestionGroup - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
    * Removes a QuestionGroup from the persistent store.
    *
    * @param        pk                identity of object to remove
    * @exception    ProcessingException
    */
    public void deleteQuestionGroup(QuestionGroupPrimaryKey pk)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            QuestionGroup bo = findQuestionGroup(pk);

            session = currentSession();
            tx = currentTransaction(session);
            session.delete(bo);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "QuestionGroupDAO.deleteQuestionGroup - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "QuestionGroupDAO.deleteQuestionGroup failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "QuestionGroupDAO.deleteQuestionGroup - Hibernate failed to close the Session - " +
                    exc);
            }
        }
    }
}
