/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.dao;

import com.occulue.bo.*;

import com.occulue.dao.*;

import com.occulue.exception.ProcessingException;

import com.occulue.primarykey.*;

import org.hibernate.*;

import org.hibernate.cfg.*;

import java.sql.*;

import java.util.*;
import java.util.logging.Logger;


/**
 * Implements the Hibernate persistence processing for business entity Referrer.
 *
 * @author dev@realmethods.com
 */

// AIB : #getDAOClassDecl()
public class ReferrerDAO extends BaseDAO // ~AIB
 {
    // AIB : #outputDAOFindAllImplementations()
    // ~AIB

    //*****************************************************
    // Attributes
    //*****************************************************
    private static final Logger LOGGER = Logger.getLogger(Referrer.class.getName());

    /**
     * default constructor
     */
    public ReferrerDAO() {
    }

    /**
     * Retrieves a Referrer from the persistent store, using the provided primary key.
     * If no match is found, a null Referrer is returned.
     * <p>
     * @param       pk
     * @return      Referrer
     * @exception   ProcessingException
     */
    public Referrer findReferrer(ReferrerPrimaryKey pk)
        throws ProcessingException {
        if (pk == null) {
            throw new ProcessingException(
                "ReferrerDAO.findReferrer(...) cannot have a null primary key argument");
        }

        Query query = null;
        Referrer businessObject = null;

        StringBuilder fromClause = new StringBuilder(
                "from com.occulue.bo.Referrer as referrer where ");

        Session session = null;
        Transaction tx = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            // AIB : #getHibernateFindFromClause()
            fromClause.append("referrer.referrerId = " +
                pk.getReferrerId().toString());
            // ~AIB
            query = session.createQuery(fromClause.toString());

            if (query != null) {
                businessObject = new Referrer();
                businessObject.copy((Referrer) query.list().iterator().next());
            }

            commitTransaction(tx);
        } catch (Throwable exc) {
            businessObject = null;
            exc.printStackTrace();
            throw new ProcessingException(
                "ReferrerDAO.findReferrer failed for primary key " + pk +
                " - " + exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferrerDAO.findReferrer - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
     * returns a Collection of all Referrers
     * @return                ArrayList<Referrer>
     * @exception   ProcessingException
     */
    public ArrayList<Referrer> findAllReferrer() throws ProcessingException {
        ArrayList<Referrer> list = new ArrayList<Referrer>();
        ArrayList<Referrer> refList = new ArrayList<Referrer>();
        Query query = null;
        StringBuilder buf = new StringBuilder("from com.occulue.bo.Referrer");

        try {
            Session session = currentSession();

            query = session.createQuery(buf.toString());

            if (query != null) {
                list = (ArrayList<Referrer>) query.list();

                Referrer tmp = null;

                for (Referrer listEntry : list) {
                    tmp = new Referrer();
                    tmp.copyShallow(listEntry);
                    refList.add(tmp);
                }
            }
        } catch (Throwable exc) {
            exc.printStackTrace();
            throw new ProcessingException(
                "ReferrerDAO.findAllReferrer failed - " + exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferrerDAO.findAllReferrer - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        if (list.size() <= 0) {
            LOGGER.info("ReferrerDAO:findAllReferrers() - List is empty.");
        }

        return (refList);
    }

    /**
     * Inserts a new Referrer into the persistent store.
     * @param       businessObject
     * @return      newly persisted Referrer
     * @exception   ProcessingException
     */
    public Referrer createReferrer(Referrer businessObject)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.save(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "ReferrerDAO.createReferrer - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "ReferrerDAO.createReferrer failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferrerDAO.createReferrer - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        // return the businessObject
        return (businessObject);
    }

    /**
     * Stores the provided Referrer to the persistent store.
     *
     * @param       businessObject
     * @return      Referrer        stored entity
     * @exception   ProcessingException
     */
    public Referrer saveReferrer(Referrer businessObject)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.update(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "ReferrerDAO.saveReferrer - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException("ReferrerDAO.saveReferrer failed - " +
                exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferrerDAO.saveReferrer - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
    * Removes a Referrer from the persistent store.
    *
    * @param        pk                identity of object to remove
    * @exception    ProcessingException
    */
    public void deleteReferrer(ReferrerPrimaryKey pk)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            Referrer bo = findReferrer(pk);

            session = currentSession();
            tx = currentTransaction(session);
            session.delete(bo);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "ReferrerDAO.deleteReferrer - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "ReferrerDAO.deleteReferrer failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferrerDAO.deleteReferrer - Hibernate failed to close the Session - " +
                    exc);
            }
        }
    }
}
