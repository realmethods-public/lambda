/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.primarykey;

import com.occulue.primarykey.*;

import java.util.*;


/**
 * Answer PrimaryKey class.
 *
 * @author    dev@realmethods.com
 */

// AIB : #getPrimaryKeyClassDecl() 
public class AnswerPrimaryKey extends BasePrimaryKey {
    //************************************************************************
    // Protected / Private Methods
    //************************************************************************

    //************************************************************************
    // Attributes
    //************************************************************************

    // DO NOT ASSIGN VALUES DIRECTLY TO THE FOLLOWING ATTRIBUTES.  SET THE VALUES
    // WITHIN THE Answer class.

    // AIB : #getKeyFieldDeclarations()
    public Long answerId;

    // ~AIB

    //************************************************************************
    // Public Methods
    //************************************************************************

    /**
     * default constructor - should be normally used for dynamic instantiation
     */
    public AnswerPrimaryKey() {
    }

    /**
     * single value constructor
     */
    public AnswerPrimaryKey(Object answerId) {
        this.answerId = (answerId != null) ? new Long(answerId.toString()) : null;
    }

    //************************************************************************
    // Access Methods
    //************************************************************************

    // AIB : #getKeyFieldAccessMethods()
    /**
         * Returns the answerId.
         * @return    Long
     */
    public Long getAnswerId() {
        return (this.answerId);
    }

    /**
         * Assigns the answerId.
         * @return    Long
     */
    public void setAnswerId(Long id) {
        this.answerId = id;
    }

    // ~AIB 	         	     

    /**
     * Retrieves the value(s) as a single List
     * @return List
     */
    public List keys() {
        // assign the attributes to the Collection back to the parent
        ArrayList keys = new ArrayList();

        keys.add(answerId);

        return (keys);
    }

    public Object getFirstKey() {
        return (answerId);
    }

    // ~AIB 	        
}
