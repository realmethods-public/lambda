/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.bo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import com.occulue.bo.*;

import com.occulue.primarykey.*;

import java.util.*;


/**
 * Encapsulates data for business entity TheResponse.
 *
 * @author dev@realmethods.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
// AIB : #getBOClassDecl()
public class TheResponse extends Base {
    // attributes

    // AIB : #getAttributeDeclarations( true  )
    protected Long theResponseId = null;
    public String responseText = null;
    public String gotoQuestionId = null;

    // ~AIB

    //************************************************************************
    // Constructors
    //************************************************************************

    /**
     * Default Constructor
     */
    public TheResponse() {
    }

    //************************************************************************
    // Accessor Methods
    //************************************************************************

    /**
     * Returns the TheResponsePrimaryKey
     * @return TheResponsePrimaryKey
     */
    public TheResponsePrimaryKey getTheResponsePrimaryKey() {
        TheResponsePrimaryKey key = new TheResponsePrimaryKey();
        key.setTheResponseId(this.theResponseId);

        return (key);
    }

    // AIB : #getBOAccessorMethods(true)
    /**
    * Returns the responseText
    * @return String
    */
    public String getResponseText() {
        return this.responseText;
    }

    /**
                  * Assigns the responseText
        * @param responseText        String
        */
    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    /**
    * Returns the gotoQuestionId
    * @return String
    */
    public String getGotoQuestionId() {
        return this.gotoQuestionId;
    }

    /**
                  * Assigns the gotoQuestionId
        * @param gotoQuestionId        String
        */
    public void setGotoQuestionId(String gotoQuestionId) {
        this.gotoQuestionId = gotoQuestionId;
    }

    /**
    * Returns the theResponseId
    * @return Long
    */
    public Long getTheResponseId() {
        return this.theResponseId;
    }

    /**
                  * Assigns the theResponseId
        * @param theResponseId        Long
        */
    public void setTheResponseId(Long theResponseId) {
        this.theResponseId = theResponseId;
    }

    // ~AIB

    /**
     * Performs a shallow copy.
     * @param object         TheResponse                copy source
     * @exception IllegalArgumentException         Thrown if the passed in obj is null. It is also
     *                                                         thrown if the passed in businessObject is not of the correct type.
     */
    public TheResponse copyShallow(TheResponse object)
        throws IllegalArgumentException {
        if (object == null) {
            throw new IllegalArgumentException(
                " TheResponse:copy(..) - object cannot be null.");
        }

        // Call base class copy
        super.copy(object);

        // Set member attributes

        // AIB : #getCopyString( false )
        this.theResponseId = object.getTheResponseId();
        this.responseText = object.getResponseText();
        this.gotoQuestionId = object.getGotoQuestionId();

        // ~AIB 
        return this;
    }

    /**
     * Performs a deep copy.
     * @param object         TheResponse                copy source
     * @exception IllegalArgumentException         Thrown if the passed in obj is null. It is also
     *                                                         thrown if the passed in businessObject is not of the correct type.
     */
    public TheResponse copy(TheResponse object) throws IllegalArgumentException {
        if (object == null) {
            throw new IllegalArgumentException(
                " TheResponse:copy(..) - object cannot be null.");
        }

        // Call base class copy
        super.copy(object);

        copyShallow(object);

        // Set member attributes

        // AIB : #getCopyString( true )
        // ~AIB 
        return (this);
    }

    /**
     * Returns a string representation of the object.
     * @return String
     */
    public String toString() {
        StringBuilder returnString = new StringBuilder();

        returnString.append(super.toString() + ", ");

        // AIB : #getToString( false )
        returnString.append("theResponseId = " + this.theResponseId + ", ");
        returnString.append("responseText = " + this.responseText + ", ");
        returnString.append("gotoQuestionId = " + this.gotoQuestionId + ", ");

        // ~AIB 
        return returnString.toString();
    }

    public java.util.Collection<String> getAttributesByNameUserIdentifiesBy() {
        Collection<String> names = new java.util.ArrayList<String>();

        return (names);
    }

    public String getIdentity() {
        StringBuilder identity = new StringBuilder("TheResponse");

        identity.append("::");
        identity.append(theResponseId);

        return (identity.toString());
    }

    public String getObjectType() {
        return ("TheResponse");
    }

    //************************************************************************
    // Object Overloads
    //************************************************************************
    public boolean equals(Object object) {
        Object tmpObject = null;

        if (this == object) {
            return true;
        }

        if (object == null) {
            return false;
        }

        if (!(object instanceof TheResponse)) {
            return false;
        }

        TheResponse bo = (TheResponse) object;

        return (getTheResponsePrimaryKey().equals(bo.getTheResponsePrimaryKey()));
    }

    // ~AIB
}
