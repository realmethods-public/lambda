<!--
    Copyright (c) 2017 
   
    This file is part of cloudMigrate.
   
        cloudMigrate is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.
   
        cloudMigrate is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.
   
        You should have received a copy of the GNU General Public License
        along with cloudMigrate.  If not, see <http://www.gnu.org/licenses/>.
        
    Contributors:
        cloudMigrate Inc - initial API and implementation
 -->
<!-- connection pool properties -->

<!-- ==================================================================== -->
<!-- Important Note:                                                      -->

<!-- As of release 4.0, database connection pooling is not used by        -->
<!-- generated DAOs.  Instead, Hibernate is used.  If you would like      -->
<!-- to continue to consume database resources through the framework's    -->
<!-- connection pool mechanism, read the instructions below.              -->

<!-- Although Hibernate is now the preferred persistence mechanism, you   -->
<!-- can still leverage the connection pooling of JMS, MQ, and LDAP       -->
<!-- connections                                                          -->
<!-- ==================================================================== -->

<!-- 
    Database related connections

A database connection can be either directly to a database 
via a driver and URL, or it can be indirect to a datasource.  
The option to pool or not pool is available under either scenario, 
but is not required.  The following parameters related to 
direct database access pooling:

   maxCapacity
   allowShrinking
   driver
   capacityIncrement
   initialCapacity

 Direct DB Connection
   req'd parameters:
       connectionClassName
       driver
       url
       user
       password

 Datasource Connection
   req'd parameters:
       connectionClassName
       JNDIDataSourceName

   optional parameters:
       user
       password
       usingDataSourcePooling: if not
           present defaults to false
-->

	
<connectionpools>

<!-- Oracle example
<connectionpool 
        name="TheDBConnection"
        connectionClassName="com.cloudmigrate.framework.integration.objectpool.db.OracleConnectionImpl"
        user="scott"
        password="tiger"
         driver="oracle.jdbc.driver.OracleDriver"
        url="jdbc:oracle:oci8:@dev"	    
        maxCapacity="10"
        initialCapacity="5"
        capacityIncrement="2"
        allowShrinking="true"   
        maxConnectionAttempts=""
	    waitUntilAvailable="true"				        
    />
-->

<!-- DB2 example
	
    <connectionpool name="TheDB2Connection"
	connectionClassName="com.cloudmigrate.framework.integration.objectpool.db.DB2ConnectionImpl"
	user=""
	password=""
	driver="COM.ibm.db2.jdbc.app.DB2Driver"
	url="jdbc:db2:RM_POC2"
    initialCapacity="1"
	maxCapacity="3"
	capacityIncrement="1"
	allowShrinking="true"
    />
-->

<!-- DataSource example 

    <connectionpool name="OracleDS"
	connectionClassName="com.cloudmigrate.framework.integration.objectpool.db.OracleConnectionImpl"
	user=""
	password=""
	usingDataSourcePooling="TRUE"
	JNDIDataSourceName="MyOracleDataSource"
    />
-->


<!-- Sybase example

    <connectionpool name="TheSybaseConnection"
	connectionClassName="com.cloudmigrate.framework.integration.objectpool.db.SybaseConnectionImpl"
	user="dba"
	password="sql"
	driver="com.sybase.jdbc.SybDriver"
	url="jdbc:sybase:Tds:localhost:2638/PROOFOFCONCEPT"
	initialCapacity="1"
	maxCapacity="3"
	capacityIncrement="1"
	allowShrinking="true"
    />
-->

<!-- MS SQL Server example 

    <connectionpool name="TheSQLServerConnection"
	connectionClassName="com.cloudmigrate.framework.integration.objectpool.db.MSSQLServerConnectionImpl"
	user=""
	password=""
	driver="com.microsoft.jdbc.sqlserver.SQLServerDriver"
	url="jdbc:microsoft:sqlserver://LITTLEGUY_NT:1433"
	initialCapacity="1"
	maxCapacity="2"
	capacityIncrement="1"
    waitUntilAvailable="true"
	allowShrinking="true"
    />
-->

<!-- JMS related connections

 reqd parameters:
   connectionClassName
   JNDIName

 optional parameters:
   factoryName - if not specified, uses the
       JMS_TOPIC_FACTORY JMS_QUEUE_FACTORY
       parameter from framework.properties
       depending on the connection class type

 Note : If you uncomment, these JMS connections will have to
 be externally created within your app.
 server.


    <connectionpool name="FrameworkEventJMS"
	connectionClassName="com.cloudmigrate.framework.integration.objectpool.jms.JMSTopicImpl"
	maxCapacity="10"
	allowShrinking="true"
	capacityIncrement="1"
	initialCapacity="2"
	JNDIName="topic/eventTopic"
	factoryName="ConnectionFactory"
    />

    <connectionpool name="FrameworkTaskExecutionJMS"
	connectionClassName="com.cloudmigrate.framework.integration.objectpool.jms.JMSTopicImpl"
	maxCapacity="10"
	allowShrinking="true"
	capacityIncrement="1"
	initialCapacity="2"
	JNDIName="topic/taskTopic"
	factoryName="ConnectionFactory"
    />

    <connectionpool name="FrameworkBusinessObjectNotificationJMS"
	connectionClassName="com.cloudmigrate.framework.integration.objectpool.jms.JMSTopicImpl"
	maxCapacity="10"
	allowShrinking="true"
	capacityIncrement="1"
	initialCapacity="2"
	JNDIName="topic/notifyTopic"
	factoryName="ConnectionFactory"
    />
-->

<!-- LDAP related connections - add fields
     specific to your application requirements.

 reqd parameters:

   connectionClassName
   java.naming.factory.initial
   java.naming.provider.url
   java.naming.security.authentication
   java.naming.security.principal
   java.naming.security.credentials
   ldap_user_key
   ldap_password_key
   ldap_role_key
   ldap_root_dn
   use_password_encryption

    <connectionpool name="MyLDAPConnection"
      connectionClassName="com.cloudmigrate.framework.integration.objectpool.ldap.LDAPConnectionImpl"
      maxCapacity="10"
      allowShrinking="true"
      capacityIncrement="1"
      initialCapacity="1"
      java.naming.factory.initial="com.sun.jndi.ldap.LdapCtxFactory"
      java.naming.provider.url="ldap://localhost:389"
      java.naming.security.authentication="simple"
      java.naming.security.principal="uid="admin,ou="Directory Administrators,o="Airius.com"
      java.naming.security.credentials="letmein2"
      use_password_encryption="true"
      ldap_user_key="uid"
      ldap_password_key="userpassword"
      ldap_role_key="objectclass"
      ldap_root_dn="ou=People,ou=ACME,ou=CEI,o=Airius.com"
      ldap_timeout_value="5000"
    />
-->

   
<!-- MQ related connections

 	backupMQPoolName - the name of the pooled MQ Connection to use
       in the event the primary is either unavailable or unusable

	errorLogHandlerName - the name of the Log Handler to use, as
       found in the log.properties file.  If empty, or the name
       is not found, uses the FrameworkDefaultLogger

	totalPrimaryFailures - number of times the primary can fail to the
       backupMQPoolName, until the backup is considered the first
       choice and the primary is ignored.  -1 means continuously try.

    <connectionpool name="SampleSendQueue"
   	connectionClassName="com.cloudmigrate.framework.integration.objectpool.mq.MQConnectionImpl"
	initialCapacity="1"
	maxCapacity="5"
	capacityIncrement="2"
	allowShrinking="true"
	queueName="anyQueue"
	queueManagerName="anyQueueManager"
	hostName="UserHost"
    port="12345"
    channel="UserChannel"
	backupMQPoolName="UserBackupQueue"
	errorLogHandlerName=""
	totalPrimaryFailures="-1"
    />
-->

</connectionpools>
